package com.andrewrp.movie.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andrewrp.movie.R;
import com.andrewrp.movie.data.model.Trailer;

import java.util.ArrayList;

public class TrailerAdapter extends RecyclerView.Adapter<TrailerHolder> {

    private ArrayList<Trailer> trailers;
    ListOnClickTrailer onCLickTrailer;

    public TrailerAdapter(ArrayList<Trailer> trailers) {
        this.trailers = trailers;
    }

    @NonNull
    @Override
    public TrailerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.desc_adapter,viewGroup,false);
        TrailerHolder holder = new TrailerHolder(view);
        onCLickTrailer = (ListOnClickTrailer) viewGroup.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final TrailerHolder holder, int i) {
        final Trailer trailer = trailers.get(i);
        holder.trailerTitle.setText(trailer.getName());
        holder.trailerType.setText(trailer.getType());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCLickTrailer.TrailerClick(trailer);
            }
        });
    }

    @Override
    public int getItemCount() {
        return trailers.size();
    }

    public interface ListOnClickTrailer{
        void TrailerClick(Trailer trailer);


    }
}

