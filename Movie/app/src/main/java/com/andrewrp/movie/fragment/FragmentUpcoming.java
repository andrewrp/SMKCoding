package com.andrewrp.movie.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.andrewrp.movie.R;
import com.andrewrp.movie.adapter.MovieAdapter;
import com.andrewrp.movie.data.Api;
import com.andrewrp.movie.data.model.Movie;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentUpcoming extends Fragment {

    ImageView imageView;
    TextView mtitle;
    RecyclerView recyclerView;
    MovieAdapter adapter;
    ArrayList<Movie> movies = new ArrayList<>();
    ProgressBar progressBar;


    public FragmentUpcoming() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_movie, container, false);

        imageView = view.findViewById(R.id.imgPoster);
        mtitle = view.findViewById(R.id.mTitle);
        recyclerView = view.findViewById(R.id.listMovie);
        progressBar = view.findViewById(R.id.progresssBar);


        fastAndroidNetworking();

        return view;
    }

    private void fastAndroidNetworking() {
        progressBar.setVisibility(View.VISIBLE);
        movies.clear();

        AndroidNetworking.get(Api.MOVIE_UPCOMING)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                Movie movie = new Movie();
                                movie.setId(jsonObject.optInt("id"));
                                movie.setPoster(jsonObject.optString("poster_path"));
                                movie.setTitle(jsonObject.optString("title"));
                                movie.setSynopsis(jsonObject.optString("overview"));
                                movie.setRelease_date(jsonObject.optString("release_date"));
                                movie.setRating(jsonObject.optString("vote_average"));
                                movies.add(movie);
                            }

                            adapter = new MovieAdapter(movies);
                            recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), 2));
                            recyclerView.setAdapter(adapter);
                            progressBar.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.i("_err", anError.toString());
                    }
                });

    }

}
