package com.andrewrp.movie.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andrewrp.movie.R;
import com.andrewrp.movie.data.Api;
import com.andrewrp.movie.data.model.Movie;
import com.andrewrp.movie.utils.DownloadImage;

import java.util.ArrayList;

import static android.media.CamcorderProfile.get;

public class MovieAdapter extends RecyclerView.Adapter<MovieHolder> {

    private ArrayList<Movie> movies;
    private ListOnClick mListener;

    public MovieAdapter(ArrayList<Movie> listMovie) {
        this.movies = listMovie;
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_adapter, parent, false);

        MovieHolder holder = new MovieHolder(view);
        mListener = (ListOnClick) parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
        final Movie movie = movies.get(position);
        holder.mtitle.setText(movie.getTitle());
        DownloadImage.picasso(Api.POSTER_PATH + movies.get(position).getPoster(),
                holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.mClick(movie);
            }
        });

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public interface ListOnClick {
        void mClick(Movie movie);
    }
}
