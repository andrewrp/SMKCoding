package com.andrewrp.movie;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

//import com.andrewrp.movie.adapter.TrailerAdapter;
import com.andrewrp.movie.adapter.TrailerAdapter;
import com.andrewrp.movie.data.Api;
import com.andrewrp.movie.data.model.Trailer;
import com.andrewrp.movie.utils.DownloadImage;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MovieDetail extends AppCompatActivity implements TrailerAdapter.ListOnClickTrailer{
//    public class MovieDetail extends AppCompatActivity {

    TextView movietitle, desc_film, relase, vote, runtime;
    ImageView cover,posterr;
    TrailerAdapter adapter;
    ProgressBar load;
    RecyclerView recyclerView;
    ArrayList<Trailer> trailers = new ArrayList<>();
    String Film_ID,rating,relase_date,duration;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        movietitle = findViewById(R.id.trailer_title);
        cover = findViewById(R.id.toolbarimage);
        desc_film = findViewById(R.id.synopsis);
        relase = findViewById(R.id.relase_date);
        vote = findViewById(R.id.rating);
        load = findViewById(R.id.load);
        recyclerView = findViewById(R.id.list_trailer);
        runtime = findViewById(R.id.runtime);
        posterr = findViewById(R.id.thumbnail);

        Bundle extras = getIntent().getExtras();
        String title = extras.getString("title");
        String synopsis = extras.getString("synopsis");
        String poster = extras.getString("image");
        String release_date = extras.getString("release_date");
        String rating = extras.getString("rating");

        String id = extras.getString("ID");

        CollapsingToolbarLayout a;
        a = findViewById(R.id.collapsing);
        a.setTitle(title);
        Film_ID = id;

        DownloadImage.picasso(Api.POSTER_PATH + poster , posterr);
        DownloadImage.picasso(Api.POSTER_PATH + poster , cover);

        desc_film.setText(synopsis);
        relase.setText(release_date);
        vote.setText(rating + " / 10");

        FastNetwork();

        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void FastNetwork() {
        trailers.clear();
        AndroidNetworking.get(Api.MOVIE_VIDEO_BASE + Film_ID + Api.MOVIE_DESC).setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        duration = String.valueOf(response.optString("runtime")  + " Minutes");
                        runtime.setText(duration);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.i("_error","error");
                    }
                });

        AndroidNetworking.get(Api.MOVIE_VIDEO_BASE + Film_ID + Api.MOVIE_VIDEO_URL).setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("results");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Log.i("name", jsonObject.optString("name"));

                                Trailer trailer = new Trailer();
                                trailer.setName(jsonObject.optString("name"));
                                trailer.setType(jsonObject.optString("type"));
                                trailer.setKey(jsonObject.optString("key"));
                                trailers.add(trailer);
                            }
                            adapter = new TrailerAdapter(trailers);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MovieDetail.this);
                            recyclerView.setLayoutManager(linearLayoutManager);
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        load.setVisibility(View.GONE);

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }


    @Override
    public void TrailerClick(Trailer trailer) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + trailer.getKey())));
    }


}

