package com.andrewrp.movie.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.andrewrp.movie.R;

public class TrailerHolder extends RecyclerView.ViewHolder {

    TextView trailerTitle,trailerType;

    public TrailerHolder(@NonNull View itemView) {
        super(itemView);
        trailerTitle = itemView.findViewById(R.id.trailer_title);
        trailerType = itemView.findViewById(R.id.trailer_type);
    }
}

