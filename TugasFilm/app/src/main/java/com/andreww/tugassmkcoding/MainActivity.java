package com.andreww.tugassmkcoding;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
    CardView meg,it,sinyal,rampage,breathe;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        meg = findViewById(R.id.themeg);

        meg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent themeg = new Intent(MainActivity.this, Meg.class);
                startActivity(themeg);
            }
        });

        it = findViewById(R.id.it);

        it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(MainActivity.this, IT.class);
                startActivity(it);
            }
        });

        sinyal = findViewById(R.id.sinyal);
        sinyal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sinyal = new Intent(MainActivity.this, susahSinyal.class);
                startActivity(sinyal);
            }
        });

        rampage = findViewById(R.id.rampage);
        rampage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent rampage = new Intent(MainActivity.this, Rampage.class);
                startActivity(rampage);
            }
        });

        breathe = findViewById(R.id.breathe);
        breathe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent breathe = new Intent(MainActivity.this, dontBreathe.class);
                startActivity(breathe);
            }
        });

        getSupportActionBar().setTitle("List Film");
    }
}
